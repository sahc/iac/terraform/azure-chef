resource "azurerm_storage_account" "ai_storage" {
  name 			= "aichef1"
  resource_group_name 	= "${azurerm_resource_group.chef_rg.name}"
  location 		= "${var.location}"
  account_tier = "Standard"
  account_replication_type = "LRS"

  tags {
    group = "${var.group_name}"
  }

}

resource "azurerm_storage_container" "ai_cont" {
  name 			= "vhds"
  resource_group_name 	= "${azurerm_resource_group.chef_rg.name}"
  storage_account_name 	= "${azurerm_storage_account.ai_storage.name}"
  container_access_type = "private"
}
