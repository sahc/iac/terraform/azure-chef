resource "azurerm_public_ip" "ai_pip" {
  name 				= "AI-Chef-PIP"
  location 			= "${var.location}"
  resource_group_name 		= "${azurerm_resource_group.chef_rg.name}"
  public_ip_address_allocation 	= "static"

  tags {
    group = "${var.group_name}"
  }
}

resource "azurerm_network_interface" "public_nic" {
  name 		      = "AI-Chef-POC"
  location 	      = "${var.location}"
  resource_group_name = "${azurerm_resource_group.chef_rg.name}"
  network_security_group_id = "${azurerm_network_security_group.nsg_chef.id}"

  ip_configuration {
    name 			= "AI-Chef-Private"
    subnet_id 			= "${azurerm_subnet.ai_subnet_1.id}"
    private_ip_address_allocation = "dynamic"
    public_ip_address_id	= "${azurerm_public_ip.ai_pip.id}"
  }
  tags {
    group = "${var.group_name}"
  }
}